# Exercise 40: Start EFK (Elastic - Fluentd - Kibana)

1. Change directory:
```bash
cd ~/workshop/exercises/kubernetes/exercise40
```

2. Turn on addons for efk
```bash
minikube addons nable efk
```

3. Check if all pods for efk are working
```bash
kubectl get all -n kube-system
```

4. Connect to kibana application
```bash
minikube service kibana-logging -n kube-system
```

5. Create pod counter
```bash
kubectl apply -f log_counter_pod.yaml
```

6. Check logs in kibana

7. Create php-docker application:
```bash
kubectl apply -f php-docker.yaml
```

8. Expose service:
```bash
minikube service php-docker --url
while true; do curl $(minikube service php-docker --url )/randtest.php ; sleep .3; done
```

9. Check logs in kibana

10. Clean:
```bash
kubectl delete -f log_counter_pod.yaml
kubectl delete -f php-docker.yaml
```

[go to home](../../../README.md)

[go to next](../exercise41/README.md)
