# Exercise 37: Test emptydir

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise37/
```

2. Create a new namespace test-emptydir
```bash
kubectl create namespace test-emptydir
kubens test-emptydir
```

3. Create a pod with EmptyDir volume
```bash
kubectl apply -f test-emptydir.yaml
```

4. Connect to the container inside POD
```bash
kubectl exec -it test-emptydir-demo -- bash
```

5. Write current date in index.html file
```bash
date > /usr/share/nginx/html/index.html
```

6. Exit the container (Ctrl-D) and create port-forward to POD
```bash
kubectl port-forward test-emptydir-demo 8080:80
curl 127.0.0.1:8080
```

7. In the second terminal login to minikube and kill container
```bash
minikube ssh
docker kill $(docker ps | grep k8s_nginx_test-emptydir-demo_test-emptydir | awk '{print $1}')
kubectl get all
```

8. Once the container is recreated and when the Pod has status Running, check application
```bash
curl 127.0.0.1:8080
```

9. Clean
```bash
kubens -
kubectl delete namespace test-emptydir
```


[go to home](../../../README.md)

[go to next](../exercise38/README.md)
