# Exercise 42: Create a new user


1. Create a cert directory
```bash
mkdir ~/cert && cd ~/cert
```

2. Generate a cert for new user
* Create key file
```bash
openssl genrsa -out test-user.key 2048
```
* Create a CSR file
```bash
openssl req -new -key test-user.key -out test-user.csr -subj "/CN=test-user/O=group1"
```
ca.crt and ca.key can be found in directory:  
* kuberentes - /etc/kubernetes/pki/
* minikube - ~/.minikube

* Create a crt file
```bash
openssl x509 \
  -req \
  -in test-user.csr \
  -CA ~/.minikube/ca.crt \
  -CAkey ~/.minikube/ca.key \
  -CAcreateserial \
  -out test-user.crt \
  -days 500
```

3. Add a user cert to kubernetes
```bash
kubectl config set-credentials test-user\
        --client-certificate=test-user.crt\
        --client-key=test-user.key
```

4. Set a new context
```bash
kubectl config set-context test-user-context \
  --cluster=minikube \
  --user=test-user
```

5. Check configuration settings
```bash
kubectl config view
```

6. Change context in kubectl and check objects 
```bash
kubectl config use-context test-user-context
kubectl config current-context
kubectl get all
```





[go to home](../../../README.md)

[go to next](../exercise43/README.md)
