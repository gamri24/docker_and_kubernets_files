# Exercise 9: Get information about all PODs configuration options

1. List the fields for supported resources.

 This command describes the fields associated with each supported API resource.
 Fields are identified via a simple JSONPath identifier: 
 kubectl explain <type>.<fieldName>[.<fieldName>]
 like: 

```bash
kubectl explain pod
```

2. Get information about POD spec:
```bash
kubectl explain pod.spec
```

[go to home](../../../README.md)

[go to next](../exercise10/README.md)
