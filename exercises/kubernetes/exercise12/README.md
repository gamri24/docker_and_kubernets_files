# Exercise 12:  Get information about horizontalpodautoscalers


## Print  all resources and api versions
1. Print all api resources
```bash
kubectl api-resources
```

2. Print all api versions
```
kubectl api-versions
```

## Compare changes between version on horizontalpodautoscaler resource
1. Print resource
```bash
kubectl explain horizontalpodautoscalers
```
2. Print options for spec for version v1
```bash
kubectl explain hpa.spec --api-version=autoscaling/v1
```

3. Print options for spec for version v2beta1
```bash
kubectl explain hpa.spec --api-version=autoscaling/v2beta1
```

4. Which api version needs to be used to set hpa over memory metrics usage ? 

[go to home](../../../README.md)

[go to next](../exercise13/README.md)
