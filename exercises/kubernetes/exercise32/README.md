# Exercise 32: Application with init container


1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise32/
```

2. Create a namespace init-contaners
```bash
kubectl create namespace init-contaners
kubens init-contaners
```

3. Create an application
```bash
kubectl apply -f init-containers.yaml
```

4. Print all objects
```bash
kubectl get all
```

5. Expose the Pod 
```bash
kubectl expose pod init-demo --type=NodePort --port=80
minikube service init-demo -n init-contaners
```

6. Check logs: 
```bash 
kubectl logs init-demo
```

7. Clean
```bash
kubens -
kubectl delete namespace init-contaners
```

[go to home](../../../README.md)

[go to next](../exercise33/README.md)
