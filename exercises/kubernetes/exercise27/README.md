# Exercise 27: Get information about the variable metrics in HorizontalPodAutoscaler

```bash
kubectl explain hpa.spec
```


[go to home](../../../README.md)

[go to next](../exercise28/README.md)
