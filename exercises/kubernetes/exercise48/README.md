# Exercise 48: Install Prometheus

1. Install Prometheus using helm 
```bash 
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
```

2. Create new service for minikube: 
```bash 
kubectl expose service prometheus-server \
  --type=NodePort \
  --target-port=9090 \
  --name=prometheus-server-np
kubectl expose service prometheus-alertmanager \
  --type=NodePort \
  --target-port=9093 \
  --name=prometheus-alertmanager-np
```

3. Check service 
```bash 
minikube service prometheus-server-np
```

4. Check metrics in prometheus in graph:

```bash
sum by (pod) (container_memory_usage_bytes{pod=~"^test-app.*"})
sum by (pod) (container_cpu_usage_seconds_total{pod =~"^test-app.*"})
```

5. In second terminal:
```bash
minikube service prometheus-alertmanager-np
```



[go to home](../../../README.md)

[go to next](../exercise49/README.md)
