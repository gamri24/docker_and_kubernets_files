# Exercise 20: Cronjob with cowsay application

1. Create namespace test-job
```bash
kubectl create namespace test-job
kubens test-job
```

2. Change the directory 
```bash
cd ~/workshop/exercises/kubernetes/exercise20/
```

3. Create cronjob using your cowsay image
```bash
kubectl create cronjob cowsays --image=registry.gitlab.com/greenitnet/cowsays:latest --schedule="*/1 * * * *"  --restart=OnFailure --
```

or

```bash
kubectl apply -f cronjob_cowsay.yaml
```

4. Watch pods created by cronjob
```bash
watch kubectl get pods
```

5. Check logs
```bash
kubectl logs cowsays-....
```
6. Clean
```bash
kubens -
kubectl delete namespace test-job
```

[go to home](../../../README.md)

[go to next](../exercise21/README.md)
