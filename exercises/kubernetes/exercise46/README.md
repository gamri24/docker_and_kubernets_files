# Exercise 46: Create a secret

1. Check if we are in the correct namespace test-security
```bash
kubens -c
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise46/
```
3. Create a secret
```bash
kubectl create -f mysecret.yaml
```

4. Create a pod with mounted secret
```bash
kubectl create -f busybox-secret.yaml
```

5. Chceck POD
```bash
kubectl exec -it busybox-secret -- sh
```

6. Create POD with secret as env variable
```bash
kubectl create -f busybox-secret-env.yaml
```

7. Check
```bash
kubectl exec busybox-secret-env -- env
```

8. Leave namespace for next exercise

[go to home](../../../README.md)

[go to next](../exercise47/README.md)
