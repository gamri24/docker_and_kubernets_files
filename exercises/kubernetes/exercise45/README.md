# Exercise 45: Turn off capabilities

1. Check if we are in the correct namespace test-security
```bash
kubens -c
```
2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise45/
```

3. Create a pod
```bash
kubectl create -f myapp.yaml
```

4. Check capabilities
```bash
kubectl exec myapp -- grep Cap /proc/1/status
```

5. Create a pod with capabilities
```bash
kubectl create -f myapp-cap.yaml
```

6. Check the pod
```bash
kubectl exec myapp-cap -- grep Cap /proc/1/status
```

7. Leave namespace for next exercise

[go to home](../../../README.md)

[go to next](../exercise46/README.md)
