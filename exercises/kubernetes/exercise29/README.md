# Exercise 29: Use an HPA based on Memory

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise29/
```

2. Create new HPA
```bash
kubectl apply -f hpa-v2-memory.yaml
```
3. Start a load-generator (benchmark)
```bash
kubectl run -it load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
```

4. Watch Pod changes
```bash
watch -d -n 1 "kubectl get hpa"
```

5. Clean
```bash
kubens -
kubectl delete namespace hpa-test
```

[go to home](../../../README.md)

[go to next](../exercise30/README.md)
