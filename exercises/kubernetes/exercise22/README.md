# Exercise 22: Rolling strategy

1. Create a namespace test-deploy
```bash
kubectl create namespace test-deploy
kubens test-deploy
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise22
```

3. Create the first application
```bash
kubectl apply -f app-v1.yaml
```

4. Watch pods
```bash
watch kubectl get pod
```

5. In the second terminal check an application
```bash
while true; do sleep 0.1; curl $(minikube service my-app --url -n test-deploy); done
```

6. In the third terminal create the second application
```bash
kubectl apply -f app-v2.yaml
```

7. Clean
```bash
kubectl delete all -l app=my-app
```

[go to home](../../../README.md)

[go to next](../exercise23/README.md)
