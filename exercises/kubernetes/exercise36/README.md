# Exercise 36: Ingress test


1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise36/
```

2. Turn on Ingress addons
```bash
minikube addons enable ingress
```

3. Create the first application
```bash
kubectl create deployment web --image=gcr.io/google-samples/hello-app:1.0
kubectl expose deployment web --type=NodePort --port=8080
```

4. Check what it returns:
```bash
curl $(minikube service web --url)
```

5. Create an ingress
```bash
kubectl apply -f example-ingress.yaml
```

6. Check ingress
```bash
kubectl get ingress
```

7. Check what it returns using ingress address:
```bash
curl 192.168.49.2/check
```

8. Create the second application
```bash
kubectl create deployment web2 --image=gcr.io/google-samples/hello-app:2.0
kubectl expose deployment web2 --type=NodePort --port=8080
```

9. Correct an ingress configuration file by adding:
```bash
      - path: /v2
        pathType: Prefix
        backend:
          service:
            name: web2
            port:
              number: 8080
```

10. Update the ingress
```bash
kubectl apply -f example-ingress.yaml
```

11. Check the exposed application:
```bash
curl 192.168.49.2/check
curl 192.168.49.2/v2/check
```

12. Clean
```bash
kubectl delete all -l app=web
kubectl delete all -l app=web2
kubectl delete -f example-ingress.yaml
```

[go to home](../../../README.md)

[go to next](../exercise37/README.md)
