# Exercise 47: Network policy - restrictied access from pod

1. Go to directory:
```bash
cd ~/workshop/exercises/kubernetes/exercise47
```

2. Enable calico network plugin
```bash
minikube stop

minikube start \
  --cpus 6 \
  --memory 6g \
  --driver=docker \
  --network-plugin=cni \
  --cni=calico
```

3. Create a namespace if you don't have it already:
```bash
kubectl create namespace test-security
kubens test-security
```
or
```bash
kubens -c
kubens test-security
```

4. Create deployment with nginx image
```bash
kubectl create deployment nginx --image=nginx
kubectl  expose deployment nginx --port=80
```

5. Checking connection from diffrent pod:
```bash
kubectl run -it load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget --spider http://nginx.com; done"
```

5. In second terminal, add network policy rule:
```bash
kubectl apply -f nginx-policy.yaml
```

6. Correct label for load-generator to all connction to nginx app:
```bash
kubectl get pods --show-labels
kubectl label pod load-generator "access=true"
```

7. Cleaning:
```bash
kubens -
kubectl delete namespace test-security
```

[go to home](../../../README.md)

[go to next](../exercise48/README.md)
