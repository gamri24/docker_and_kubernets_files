# Exercise 39: LAMP application

1. Create a namespace test-subpath
```bash
kubectl create namespace test-subpath
kubens test-subpath
```
2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise39/
```
3. Create an application LAMP
```bash
kubectl apply -f test-lamp-subpath.yaml
```

4. Login to minikube and check volume-subpaths
```bash
minikube ssh
sudo -i
cd /var/lib/kubelet/pods/$(docker ps | grep wordpress | awk '{print $10}' | awk -F "_" '{print $5}' | uniq | grep -v ^$)/volume-subpaths
```

5. Exit from minikube

6. Clean
```bash
kubens -
kubectl delete namespace test-subpath
```

[go to home](../../../README.md)

[go to next](../exercise40/README.md)
