# Exercise 1: Run minikube

1. Start minikube
```bash
minikube start --cpus 6 --memory 6g --driver=docker
```
![minikube-start](images/minikube_start2.png)

2. Checking instalation
```bash
kubectl get nodes
```
or using minkube
```bash
minikube kubectl -- get nodes
```

3. Connect to docker registry inside minikube
```bash
minikube docker-env
```
4. Add environment variable
```bash
eval $(minikube -p minikube docker-env)
```
5. Checking
```bash
docker images
```

[go to home](../../../README.md)

[go to next](../exercise2/README.md)
