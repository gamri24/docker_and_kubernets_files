# Exercise 17: Using labels in deployment

1. Show all objects with labels
```bash
kubectl get all --show-labels
```

2. Show a relation between them:
```bash
$ kubectl get all -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}' -l k8s-app=test-app
```

3. Watch pods
```bash
watch -n 1 -d "kubectl get pod --show-labels"
```

4. In the second terminal delete label
```bash
kubectl label pod test-app...  k8s-app-
```

5. Show all objects with labels
```bash
kubectl get all --show-labels
kubectl get pods --selector k8s-app
```

6. Show again relation between them
```bash
kubectl get all -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}' -l k8s-app=test-app
```

7. Add label back

```bash
kubectl label pod test-app... k8s-app=test-app
```

8. What happened with the new POD for test-app ?


[go to home](../../../README.md)

[go to next](../exercise18/README.md)
