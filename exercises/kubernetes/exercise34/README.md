# Exercise 34: Create headless service for nginx application

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise34/
```

2. Create a headless service
```bash
kubectl apply -f svc-headless-my-application.yaml
```

3. Create a Pod with dns tools
```bash
$ kubectl run dnsutils \
  --image=registry.gitlab.com/greenitnet/dnsutils:latest \
  -- sleep 600
```

4. Check return dns values for services my-applicaion and my-application-headless
```bash
kubectl exec dnsutils -- dig my-application-headless A +search +short
kubectl exec dnsutils -- dig my-application A +search +short
```

5. Scale the pod to 2 replicas
```
kubectl scale deployment my-application --replicas=2
```
6. Check number of pods:
```bash
kubectl get pods
```

7. Check return dns values for services my-applicaion and my-application-headless
```bash
kubectl exec dnsutils -- dig my-application-headless A +search +short
kubectl exec dnsutils -- dig my-application A +search +short
```

[go to home](../../../README.md)

[go to next](../exercise35/README.md)
