# Exercise 6: Add labels

1. Print all pods with information about labels 
```bash
kubectl get pod --show-labels
```
2. Add label to existing Pod
```bash 
kubectl label pod busybox-cli app=busybox
```

3. Print all pods with labels again 
```bash 
kubectl get pod --show-labels
```


[go to home](../../../README.md)

[go to next](../exercise7/README.md)
