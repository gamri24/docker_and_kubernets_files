# Exercise 26: Application scale

The are two options to scale the deployment 
*  Scale by edit deployment and increaseing replicas number: 
```bash
kubectl edit deployments test-app
```

* Scale by command line
```bash
kubectl scale deployment test-app --replicas=3
```

[go to home](../../../README.md)

[go to next](../exercise27/README.md)
