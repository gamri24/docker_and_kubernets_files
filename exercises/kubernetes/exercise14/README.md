# Exercise 14: Add pod limits as container environment variable

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise14/
```

2. Create a Pod with an environment variable with limits 
```bash
kubectl apply -f env_limits.yaml
```
3. Checking a container environment variable
```bash
kubectl exec envlimit -- env | grep MEMORY | sort
```

4. Checking limits direct in cgroup:
```bash
kubectl exec envlimit  -- cat /sys/fs/cgroup/memory/memory.limit_in_bytes
```

5. Clean
```bash
kubectl delete -f env_limits.yaml
```

[go to home](../../../README.md)

[go to next](../exercise15/README.md)
