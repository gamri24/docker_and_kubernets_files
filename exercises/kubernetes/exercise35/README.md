# Exercise 35: Service my-service -> www.nobleprog.pl

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise35/
```

2. Create an ExternalName service
```bash
kubectl apply -f service-external-name.yaml
```

3. Print all services:
```bash
kubectl get svc
```

4. Check service
```bash
kubectl exec dnsutils -- dig my-service A +search

my-service.test-network.svc.cluster.local. 30 IN CNAME www.nobleprog.pl
www.nobleprog.pl.	60	IN	A	94.23.93.233
```

5. Clean
```bash
kubens -
kubectl delete namespace test-network
```


[go to home](../../../README.md)

[go to next](../exercise36/README.md)
