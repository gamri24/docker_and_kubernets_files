# Exercise 10: Check default values set for busybox POD

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise10/
```

2. Create a Pod at busybox image
```bash
kubectl create -f busybox.yaml
```

3. Print a Pod configuration file
```bash
kubectl get pods busybox -o yaml
```

[go to home](../../../README.md)

[go to next](../exercise11/README.md)
