# Exercise 13: Liveness and readiness probe

## Create application hello world writing to redis: 

1. Create Pods
```bash 
kubectl apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_flask.yaml
```

2. Print logs 
```bash 
kubectl logs -f myapp...
```

3. Print events 
```bash 
kubectl get events -w
```
4. In the second terminal print description for pod 
```bash 
kubectl describe pod myapp...
```

5. Create Pod with redis 
```bash 
kubectl apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_redis.yaml
```

6. Enable service by minikube: 
```bash 
minikube service http
```

7. Clean 
```bash 
kubectl delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_flask.yaml
kubectl delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_redis.yaml
```

[go to home](../../../README.md)

[go to next](../exercise14/README.md)
