# Exercise 28: Use an HPA based on CPU

1. Craate new namespace
```bash
kubectl create namespace hpa-test
kubens hpa-test
```

2. Start metric-server in minikube
```bash
minikube addons enable metrics-server
kubectl get all -n kube-system
```

3. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise28/
```

4. Create anapplication
```bash
kubectl apply -f php-apache.yaml
```

5. Set a Pod autoscale based on CPU
```bash
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
or
```bash
kubectl apply -g hpa-v1.yaml
```

6. Start a load-generator (benchmark)
```bash
kubectl run -i --tty load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
```
7. Watch Pod changes
```bash
watch -d -n 1 "kubectl get hpa"
```

8. How it looks in v2 version configuration format?
```bash
kubectl get hpa.v2beta2.autoscaling php-apache -o yaml
```

9. Clean
```bash
kubectl delete hpa php-apache
```

[go to home](../../../README.md)

[go to next](../exercise29/README.md)
