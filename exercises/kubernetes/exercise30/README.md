# Exercise 30: Pod with shared storage between containers

![MultiContainerPods](images/MultiContainerPods.png)

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise30/
```

2. Create a namespace test-multicontainer
```bash
kubectl create namespace test-multicontainer
kubens test-multicontainer
```

3. Create an application
```bash
kubectl apply -f multi-container.yaml
```

4. Create a service
```bash
kubectl expose deployment mc1 --type=NodePort --port=80
```

5. Expose the service
```bash
while true; do sleep 0.1; curl $(minikube service mc1 --url -n test-multicontainer); done
```

6. Check logs
```bash
kubectl logs -f mc1... 1st
kubectl logs -f mc1... 2nd
```

7. Check files with data
```bash
kubectl exec -it mc1... -c 1st -- cat /usr/share/nginx/html/index.html
kubectl exec -it mc1... -c 2nd -- cat /html/index.html
```
8. Clean
```bash
kubectl delete -f multi-container.yaml
 ```

[go to home](../../../README.md)

[go to next](../exercise31/README.md)
