# Exercise 33: Create ClusterIP service for nginx application

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise33/
```

2. Create a namespace test-network
```bash
kubectl create namespace test-network
kubens test-network
```

3. Create a service
```bash
kubectl apply -f svc-my-application.yaml
```

4. Create an application
```bash
kubectl apply -f my-application.yaml
```

5. Scale the application to 3 replicas
```bash
kubectl scale deployment my-application --replicas=3
```

6. Start a load-generator
```bash
kubectl run -i --tty load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.2; do wget -q -O /dev/null http://my-application; done"
```

7. In the second terminal watch logs
```bash
kubectl logs -f my-application..
```
or watch logs from all pods:
```bash
stern my-application
```

[go to home](../../../README.md)

[go to next](../exercise34/README.md)
