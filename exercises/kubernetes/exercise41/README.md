# Exercise 41: Start metric server

1. Enable metrics-server if you didn't already:
```bash 
minikube addons enable metrics-server
```

2. Check metrics from nodes and pods: 
```bash
kubectl top node
kubectl top pods -A
```

[go to home](../../../README.md)

[go to next](../exercise42/README.md)
