# Exercise 49: Install grafana

1. Install gragana using helm: 
```bash 
helm repo add grafana https://grafana.github.io/helm-charts 
helm repo update
helm install grafana grafana/grafana
```

2. Create new service for minikube: 
```bash 
kubectl expose service grafana \
  --type=NodePort \
  --target-port=3000 \
  --name=grafana-np
```

3. Getting password from secret: 
```bash 
kubectl get secret \
  --namespace default \
  grafana \
  -o jsonpath="{.data.admin-password}" \
  | base64 --decode ; echo
```

4. Start service 
```bash 
minikube service grafana-np
```

5. Add Prometheus Datasource
```bash 
Configuration -> Datasources  and add Prometheus (http://prometheus-server)
```

6. Upload dashboardu
```bash 
 Create (+) -> Import section
 Import via grafana.com -> id 6417
```

![schemat](images/prometheus_grafana.png)

7. Upload second dashboard
```bash 
 Import via grafana.com ->  id 1094  (opcjonalnie 3870)
```     

* Other public dashboard are avaible at https://grafana.com/grafana/dashboards

[go to home](../../../README.md)

