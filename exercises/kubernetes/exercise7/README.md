# Exercise 7: Start application from yaml file (declarative approach)

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise7/
```
2. Create Pod from file
```bash
kubectl create -f busybox.yaml
```

3. Check pods 
```bash
kubectl get pods
```

[go to home](../../../README.md)

[go to next](../exercise8/README.md)
