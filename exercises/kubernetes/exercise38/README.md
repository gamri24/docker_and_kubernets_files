# Exercise 38: Using the PVC

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise38/
```

2. Create a PVC
```bash
kubectl apply -f pv-clame.yaml
```

3. Check
* PVC
```bash
kubectl get pvc
kubectl describe pvc task-pv-claim
```
* PV
```bash
$ kubectl get pv
$ kubectl describe pv
```

4. Create a Pod with PVC
```bash
kubectl apply -f pv-pod.yaml
kubectl describe pod task-pv-pod
```

5. Clean
```bash
kubectl delete -f pv-pod.yaml
kubectl delete -f pv-clame.yaml
```



[go to home](../../../README.md)

[go to next](../exercise39/README.md)
