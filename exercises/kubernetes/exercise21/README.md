# Exercise 21: Create a POD with a configmap as volume

1. Create namespace test-configmap
```bash
kubectl create namespace test-configmap
kubens test-configmap
```

2. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise21/
```

3. Create a conifg map
```bash
kubectl apply -f my-app-nginx-configmap.yaml
```

4. Check
```bash
kubectl get configmap -o yaml
```

5. Create a deployment
```bash
kubectl apply -f my-app-nginx.yaml
```

6. Check
```bash
kubectl describe pod my-application...
```

7. Create a port-forward
```bash
kubectl port-forward my-application... 8080:80
```

8. In the second window check site
```bash
curl -v 127.0.0.1:8080
```

9. Clean
```bash
kubens -
kubectl delete namespace test-configmap
```

[go to home](../../../README.md)

[go to next](../exercise22/README.md)
