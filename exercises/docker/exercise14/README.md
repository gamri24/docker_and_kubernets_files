# Exercise 14: Dockerfile building context

## In cowsay directory create new directories for dockerfiles and context

```bash 
mkdir dockerfiles context
mv Dockerfile dockerfiles && mv entrypoint.sh context
```

## build image using new file structure 
```bash 
docker build --no-cache -t test/cowsay-dockerfile -f dockerfiles/Dockerfile context
```

[go to home](../../../README.md)

[go to next](../exercise15/README.md)
