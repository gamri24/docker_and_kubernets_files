# Exercise 4: Docker storage volume

## Create new volume and inspect it 

```bash 
docker volume create my-vol
docker volume ls
docker volume inspect my-vol
```

## Run container with apache aplication with mounted volume 

```bash 
docker run -d \
  --rm=true \
  -p 80:80 \
  --name devtest01 \
  --hostname devtest01 \
  -v my-vol:/var/www/html \
  php:7.2-apache
```


## Connect to container
```bash 
docker exec -it devtest01 bash
```
## Write hostname to index.php file inside container: 
```bash 
echo "hello from " $(hostname -f) > /var/www/html/index.php
```

## Exit from container and check application
```bash 
curl 127.0.0.1:80/index.php
```

## Stop container
```bash 
docker stop devtest01
```


## Create new container with the same volume mounted

```bash 
docker run -d \
  --rm=true \
  -p 80:80 \
  --name devtest02 \
  --hostname devtest02 \
  -v my-vol:/var/www/html \
  php:7.2-apache
```

## Check application 
```bash 
curl 127.0.0.1:80/index.php
```

## Stop container
```bash
docker stop devtest02
```

## Remove volume
```bash 
docker volume rm my-vol
```

[go to home](../../../README.md)

[go to next](../exercise5/README.md)
