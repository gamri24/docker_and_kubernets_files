# Exercise 10: Docker logs with journald driver

## Create a container, which is writing at output message every second
```bash 
docker run -d \
  --rm=true \
  --name=test-logs \
  --log-driver=journald \
  debian:wheezy \
  sh -c 'i=0; while true; do i=$((i+1)); echo "docker $i"; sleep 1; done;'
```
## Check logs using docker client
```bash
docker inspect test-log
```
## Check logs using journalctl
```bash
journalctl -f
```

## Check logs using docker client
```bash
docker logs -f test-logs
```

## Clean
```bash
docker stop test-logs
```

[go to home](../../../README.md)

[go to next](../exercise11/README.md)
