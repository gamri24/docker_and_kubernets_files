# Exercise 2: docker inspect

## Run cotainer 
```bash 
docker run -d --name test-inspect php:7.2-apache
```

## Show all informatin about container test-inspect
```bash 
docker inspect test-inspect
```

## Use format to show only information about container IP address:
```bash 
docker inspect --format='{{range .NetworkSettings.Networks }}{{ .IPAddress }} {{end}}' test-inspect
```

## Use format to show image used to start container 
```bash 
docker inspect --format='{{.Config.Image}}' test-inspect
```

[go to home](../../../README.md)

[go to next](../exercise3/README.md)
