# Exercise 5: docker storage bind

## Create php file:
```bash 
mkdir php_code
cat << EOF > php_code/index.php
<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
 <?php echo "<p>Hello World</p>"; ?>
 </body>
</html>
EOF
```

## Create container with apache application
```bash 
docker run -d \
  --rm \
  -p 80:80 \
  --name my-apache-php-app \
  -v $PWD/php_code:/var/www/html \
  php:7.2-apache
```

## Check aplication output
```bash 
curl 127.0.0.1:80/index.php
```

## Change title in file index.php and check applicaiton output
```bash 
curl 127.0.0.1:80/index.php
```

## Stop container
```bash 
docker stop my-apache-php-app
```

[go to home](../../../README.md)

[go to next](../exercise6/README.md)
