# Exercise 8: Docker inspect

## Run container test_inspect and check all information about it 
```bash 
docker run --rm -d --name test_inspect ubuntu:latest sleep 3600
docker inspect test_inspect
```

## Task to done: 
1. check docker-engine version
```bash
docker version
```
2. Check docker server information
```bash 
docker info 
```
3. Pull centos 8 image
  * check tags by skopeo or webpage at hub.docker.com
```bash 
skopeo inspect --override-os=linux docker://docker.io/centos
```
  * Pull image 
```bash 
docker pull centos:8
```

4. Check centos 8 image digest
```bash 
docker images --digests
```
5. Run container from centos 8
```bash 
docker run --rm --name="centos" centos:8 sleep 300 
```
6. Check container size
```bash 
docker ps -s 
```
7. Run container inspection
```bash 
docker inspect centos
```
8. Get ip address from container
```bash 
docker inspect --format='{{range .NetworkSettings.Networks }}{{ .IPAddress }} {{end}}' centos
```

[go to home](../../../README.md)

[go to next](../exercise9/README.md)
