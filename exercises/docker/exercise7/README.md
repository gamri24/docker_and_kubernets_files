# Exercise 7: Containers management 

## Containers
### Show all containers
```bash 
 docker ps
 docker ps -a
```
### Delete container based on debian image
```bash 
 docker ps -a --filter ancestor=debian
 docker rm <hash>
```

## Images 
### Show and remove image

```bash 
docker images
docker rmi image hash
```

## Using filters
### Delete all containers using filters
```bash 
docker rm $(docker ps -a -q --filter ancestor=debian)
docker rm $(docker ps -a -q --filter 'exited=0')
docker rm $(docker ps -a -q --filter 'exited=137')
docker rm $(docker ps -a -q)
```

### Delete images using filter
```bash 
docker rmi $(docker images -q -f dangling=true)
docker rmi $(docker images -q )
```

[go to home](../../../README.md)

[go to next](../exercise8/README.md)
