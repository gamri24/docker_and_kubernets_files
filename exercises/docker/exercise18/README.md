# Exercise 18: Run flask and redis in docker-compose

Applicaion is saving each request in the redis database
and return a number of all requests.

## Starting application

1. Go to directory with applicaion
```bash
cd ~/workshop/exercises/docker/exercise18/
```

2. Check files:
```bash
cat app.py
cat requirements.txt
cat Dockerfile
cat docker-compose.yml
```

3. Start docker-compose stack:

* Start stack:
```bash
docker-compose up
```
* Open www page [http://127.0.0.1:5000](http://127.0.0.1:5000)
* Refresh page several times
* Check images in local registry:
```bash
docker images
```
* Stop stack:
```bash
docker-compose down --rmi local
```

## Correct Dockerfile with bind mount

1. Create docker-compose.override.yml
```bash
services:
  web:
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
    depends_on:
      - "redis"
```

2. Start docker-compose stack
* Rebuild stack:
```bash
docker-compose up
```
* Open www page: [http://127.0.0.1:5000](http://127.0.0.1:5000)
* Update welcome massage in applicaion code
  ```bash
  Hello World
  ```
* Refrash www page:  [http://127.0.0.1:5000](http://127.0.0.1:5000)
* Print env variables from web service:
```bash
docker-compose run web env
```
* Stop stack:
```
docker-compose down --rmi local
```

## Add services dependcies and healthcheck

1. Correct docker-compose.override.yml file:
```bash
services:
  web:
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
    depends_on:
      - "redis"
    healthcheck:
      test: ["CMD", "curl", "-f", "http://127.0.0.1:5000"]
      interval: 30s
      timeout: 10s
      retries: 3
  client_1:
    build:
      context: .
      dockerfile: ./Dockerfiles/client/Dockerfile
    hostname: client1
    depends_on:
      web:
        condition: service_healthy
```

2. Correct Dockerfile (we need curl tool for health check)
```bash
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers curl
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000/tcp
COPY . .
CMD ["flask", "run"]
```

3. Start stack
* Rebuild stacka:
```bash
docker-compose up
```
* Open www page: [http://127.0.0.1:5000](http://127.0.0.1:5000)
* Scale servis client to 5:
```bash
docker-compose up -d --scale client_1=5
```
* Scale servis client to 1:
```bash
docker-compose up -d --scale client_1=1
```
* Stop stack:
```bash
docker-compose down --rmi local --volumes
```

[go to home](../../../README.md)
