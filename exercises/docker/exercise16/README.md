# Exercise 16: Dockerfile multi-stage build

1. Pulling repository with example app:
```bash
git clone https://github.com/tszymanski/multi-stage-build.git
```
2. Build images:
```bash
cd multi-stage-build.git
docker build -t one-stage:latest -f Dockerfile .
docker build -t multi-stage:latest -f Dockerfile_multistage .
docker build -t multi-stage-scratch:latest -f Dockerfile_multistage_scratch .
```

3. Check images size
```bash
docker image ls -f label=example=multi-stage
```

4. Check history for onestage image

```bash
docker image history one-stage

IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
580d9a3773c3        3 days ago          /bin/sh -c #(nop)  CMD ["./app"]                0B
1d273b69b751        3 days ago          /bin/sh -c go build -o app .                    2.06MB
9470164ceeb4        3 days ago          /bin/sh -c #(nop) COPY file:df60b2671a451d45…   171B
8e4e0d53dabd        3 days ago          /bin/sh -c #(nop) WORKDIR /code/                0B
fc0a21dde945        3 days ago          /bin/sh -c #(nop)  LABEL maintainer=tomasz.s…   0B
4a581cd6feb1        3 days ago          /bin/sh -c #(nop) WORKDIR /go                   0B
<missing>           3 days ago          /bin/sh -c mkdir -p "$GOPATH/src" "$GOPATH/b…   0B
<missing>           3 days ago          /bin/sh -c #(nop)  ENV PATH=/go/bin:/usr/loc…   0B
<missing>           3 days ago          /bin/sh -c #(nop)  ENV GOPATH=/go               0B
<missing>           3 days ago          /bin/sh -c set -eux;   dpkgArch="$(dpkg --pr…   363MB
<missing>           5 days ago          /bin/sh -c set -ex;  if ! command -v gpg > /…   17.5MB
```

5. Show all commands in image history:

```bash
docker history one-stage:latest --no-trunc $argv \
  | tac \
  | tr -s ' '\
  | cut -d " " -f 5- \
  | sed 's,^/bin/sh -c #(nop) ,,g' \
  | sed 's,^/bin/sh -c,RUN,g' \
  | sed 's, && ,\n  & ,g' \
  | sed 's,\s*[0-9]*[\.]*[0-9]*[kMG]*B\s*$,,g' \
  | head -n -1
```

6. Use [dive](https://github.com/wagoodman/dive) tool

* Check images from multistage build
```bash
dive one-stage:latest
dive multi-stage:latest
dive multi-stage-scratch:latest
```

[go to home](../../../README.md)

[go to next](../exercise17/README.md)
