# Exercise 17: Run wordpress application

1. Create volume for database
```bash
docker volume create vol-mysql
```
2. Run mysql container
```bash
docker run -d \
  --rm=true \
  --name mysql \
  -v vol-mysql:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=mysqlpass \
  mysql:5.6
```
3. Create volume for webfront
```bash
docker volume create vol-wordpress
```
4. Run webfron contaner
```bash
docker run -d \
  --rm=true \
  --name wordpress \
  -v vol-wordpress:/var/www/html \
  -e WORDPRESS_DB_HOST=mysql \
  -e WORDPRESS_DB_PASSWORD=mysqlpass \
  -p 80:80 \
  --link mysql:mysql \
  wordpress:5.6-apache
```
5. Open application in web browser [http://127.0.0.1](http://127.0.0.1)

6. Clean
```bash
docker stop wordpress
docker stop mysql
docker volume rm vol-wordpress
docker volume rm vol-mysql
```

![wordpress](images/wordpres_kube.png)

[go to home](../../../README.md)

[go to next](../exercise18/README.md)
