# Exercise 9: Docker exec

## Create container
```bash
docker run --rm -d -p 80:80 --name test-exec php:7.2-apache
```

## Check apache applicaion:
```bash
curl 127.0.0.1:80/index.html
```

## Start bash shell inside container and attached stdin and stdout to it
```bash
$ docker exec -it test-exec /bin/bash
```
## Update index.html file insied container and exit container:
```bash
echo "update from docker exec" >index.html
CTRL-D
```

## Check applicaion
```bash
curl 127.0.0.1:80/index.html
```

## Clean
```bash
docker stop test-exec
```
[go to home](../../../README.md)

[go to next](../exercise10/README.md)
