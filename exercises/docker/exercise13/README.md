# Exercise 13: Docker image from file

## Create directory:
```bash
mkdir cowsay
cd cowsay
```

## Create Dockerfile file
```bash
cat << EOF > Dockerfile
FROM debian
RUN apt-get update && apt-get install -y cowsay fortune
EOF
```

## Build image:
```bash
docker build -t test/cowsay-dockerfile .
```

## Run container
```bash
docker run test/cowsay-dockerfile /usr/games/cowsay "Moo"
```

## Add entrypoint variable with cowsay application
```bash
echo 'ENTRYPOINT ["/usr/games/cowsay"]' >> Dockerfile
```

## Rebuild and run again
```bash
docker build -t test/cowsay-dockerfile .
docker run test/cowsay-dockerfile "Moo"
```

## Create entrypoint script file
```bash
cat << EOF  > entrypoint.sh
#!/bin/bash
if [ \$# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay
else
   /usr/games/cowsay "\$@"
fi
EOF
```
## Correct permissions
```bash
chmod +x entrypoint.sh
```

## Correct Dockerfile
```bash
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
```

## Build image
```bash
docker build -t test/cowsay-dockerfile .
```

## Run container
```bash
docker run test/cowsay-dockerfile
docker run test/cowsay-dockerfile "Moo"
```

## Check image history
```bash
docker image history test/cowsay-dockerfile
```

[go to home](../../../README.md)

[go to next](../exercise14/README.md)
