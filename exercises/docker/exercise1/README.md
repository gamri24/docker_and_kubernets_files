# Exercise 1: Connect directly to  API

## Create container by connecting direct to API using curl 

1. Create container
```bash 
HASH=$(curl --unix-socket /var/run/docker.sock -H "Content-Type: application/json" -d '{"Image": "debian", "Cmd": ["echo", "hello world"]}' -X POST http:/v1.24/containers/create | jq -r '.Id')
```

2. Start container 
```bash 
curl --unix-socket /var/run/docker.sock -X POST http:/v1.24/containers/${HASH}/start
```

3.  Pull logs from started container
```bash 
curl --unix-socket /var/run/docker.sock "http:/v1.24/containers/${HASH}/logs?stdout=1"  | xargs -0 echo
```

## Create container by connecting direct to API using python sdk

1. Download debian image
```bash
docker pull debian
```

2. Open python3 interactive terminal:
```bash
$ python3
>>> import docker
>>> client = docker.from_env()
>>> client.containers.run("debian", ["echo", "hello", "world"])
```

[go to home](../../../README.md)

[go to next](../exercise2/README.md)
