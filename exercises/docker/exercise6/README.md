# Exercise 6: Docker storage bind with overwrite dir


## Create new container with bind local dir to /usr inside container
```bash 
docker run -d \
  --rm=true \
  --name test-bind \
  -v "$PWD:/usr" \
  php:7.2-apache
```

## Create new container with bind local passwd file to index.html file inside container
```bash 
docker run -d \
  --rm=true \
  -p 80:80 \
  --name test-bind2 \
  -v "/etc/passwd:/var/www/html/index.html:ro" \
  php:7.2-apache
```

## Check applcation output
```bash 
curl 127.0.0.1:80/index.html
```

## Stop container
```bash 
docker stop test2
```

[go to home](../../../README.md)

[go to next](../exercise7/README.md)
