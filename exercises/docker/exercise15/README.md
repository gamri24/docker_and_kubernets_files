# Exercise 15: Public remote repository

## Push image to registry
1. Create account at gitlab.com
![gitlab-login](images/gitlab_signup.png)

2. Generate ssh-key in terminal
```bash
ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/nobleprog/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/nobleprog/.ssh/id_rsa
Your public key has been saved in /home/nobleprog/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:7JzSfK+UtORSjpTW6/8hkh6Fc8w6mrmztWglm/EMVcI nobleprog@tomasz-3tlh
The key's randomart image is:
+---[RSA 3072]----+
|         .       |
|          E .    |
|           o     |
|       . o.+     |
|        S.B =    |
|       B+O.X     |
|      . OX/ . .  |
|       .=%o* . . |
|       .B+=oo..  |
+----[SHA256]-----+
```

3. Print ssh-key public key
```bash
cat .ssh/id_rsa.pub
```

3. Add ssh-key to your account
![gitlab-profil](images/gitlab_profil.png)
![gitlab-sshkey](images/gitlab_addkey.png)

4. Create token at Personal Access Tokens with read and write registry options
![gitlab-token](images/gitlab_token.png)

5. Create new project cowsay
![gitlab-newproject](images/gitlab_newrepo.png)

6. Check if registry is on in settings
![gitlab-registry](images/gitlab_enableregistry.png)

5. Commit your cowsay code to repo

6. Login to gitlab registry using Personal Access Tokens instead your password
```bash 
docker login registry.gitlab.com
```
7. Change name and tag local image: using comand docker tag:

docker tag <old image> <project_name>:<tag>
```bash
docker tag my-local-version-of-cowsay:latest registry.gitlab.com/<your account>/cowsay:v1.0.0
```
8. Push newly created image to registry
```bash
docker push registry.gitlab.com/<your account>/cowsay:v1.0.0
```

## Correct Dockerfile and push corrected image to registry with new tag
1. Correct Dockerfile for cowsay based on Best practices

Corrected Dockerfile can be find in
```bash
~/workshop/exercises/docker/exercise15/Dockerfile_final_cowsay
```

2. Build new image and push it to gitlab.com
```bash 
docker build --no-cache -t registry.gitlab.com/<your account>/cowsay:2.0.0  -f dockerfiles/Dockerfile context
```

3. Push corrected cowsay image
```bash
docker push registry.gitlab.com/<your account>/cowsay:2.0.0
```
4. Write link to your image at zoom chat
5. Try to pull image from your colleague from the group

[go to home](../../../README.md)

[go to next](../exercise16/README.md)
