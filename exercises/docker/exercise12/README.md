# Exercise 12: Docker image from command line


## Run container in interactive mode 
```bash
 docker run -it --name cowsay --hostname cowsay debian bash
```
## Install necessary packages
```bash 
apt-get update
apt-get install -y cowsay fortune
/usr/games/fortune | /usr/games/cowsay
```
## exit and commit container as image 
```bash 
exit
docker commit cowsay test/cowsayimage
```

## Run container based on new image 
```bash 
docker run test/cowsayimage /usr/games/cowsay "Moo"
```

[go to home](../../../README.md)

[go to next](../exercise13/README.md)
