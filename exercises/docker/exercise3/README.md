# Exercise 3: docker logs

## Create container writing to stdout current date
```bash 
docker run --name test -d busybox sh -c 'while true; do $(echo date); sleep 1; done'
```
## Show logs from started container 
```bash 
docker logs -f test
```
## Show logs from started container until/since options:
```bash 
date && docker logs -f --until=10s  test
date && docker logs -f --since=10s test
```

[go to home](../../../README.md)

[go to next](../exercise4/README.md)
