# Docker and Kubernetes Training

## Directories:

* presentation -- contain all presentation files
* exercises -- cantain all exercises
* examples -- contain examples directories 

## Requirements

To run all exercises you need to have the following tools in your workstation:

* docker
* dive - https://github.com/wagoodman/dive
* docker-compose - https://docs.docker.com/compose/install/
* jq - https://stedolan.github.io/jq/
* nsentry  - https://man7.org/linux/man-pages/man1/nsenter.1.html
* skopeo - https://github.com/containers/skopeo
* python3 docker - https://docker-py.readthedocs.io/en/stable/

* minikube - https://minikube.sigs.k8s.io/docs/start/ 
* stern - https://github.com/wercker/stern
* Kube-ps1 - https://github.com/jonmosco/kube-ps1


## Links

* Fluentd - https://www.fluentd.org
* Best Practices Around Production Ready Web Apps with Docker Compose - https://nickjanetakis.com/blog/best-practices-around-production-ready-web-apps-with-docker-compose
* Demystifying-containers - https://github.com/saschagrunert/demystifying-containers

## HowTo

You need to clone reposiory to correct directory in your file system, like:

```bash
git clone https://gitlab.com/greenitnet/docker_and_kubernets_files.git ~/workshop
```

## Examples list 

### Docker: 

### [Examples stack](examples/docker/example_stack_wordpress/README.md): Run wordpress application in docker-compose

### Kubernetes:

## Exercises list

### Docker:

### [Exercise 1](exercises/docker/exercise1/README.md): Connect directly to  API
### [Exercise 2](exercises/docker/exercise2/README.md): Docker inspect
### [Exercise 3](exercises/docker/exercise3/README.md): Docker logs
### [Exercise 4](exercises/docker/exercise4/README.md): Docker storage vovlume
### [Exercise 5](exercises/docker/exercise5/README.md): Docker storage bind
### [Exercise 6](exercises/docker/exercise6/README.md): Docker storage bind with overwrite dir
### [Exercise 7](exercises/docker/exercise7/README.md): Container management
### [Exercise 8](exercises/docker/exercise8/README.md): Docker inspect
### [Exercise 9](exercises/docker/exercise9/README.md): Docker exec
### [Exercise 10](exercises/docker/exercise10/README.md): Docker logs with jurnald driver
### [Exercise 11](exercises/docker/exercise11/README.md): Docker stats
### [Exercise 12](exercises/docker/exercise12/README.md): Docker image from command line
### [Exercise 13](exercises/docker/exercise13/README.md): Docker image from file
### [Exercise 14](exercises/docker/exercise14/README.md): Dockerfile building context
### [Exercise 15](exercises/docker/exercise15/README.md): Public remote repository
### [Exercise 16](exercises/docker/exercise16/README.md): Dockerfile multi-stage build
### [Exercise 17](exercises/docker/exercise17/README.md): Run wordpress application
### [Exercise 18](exercises/docker/exercise18/README.md): Run flask and redis in docker-compose

### Kubernetes:

### [Exercise 1](exercises/kubernetes/exercise1/README.md): Run minikube
### [Exercise 2](exercises/kubernetes/exercise2/README.md): Create namespace
### [Exercise 3](exercises/kubernetes/exercise3/README.md): Start dashboard
### [Exercise 4](exercises/kubernetes/exercise4/README.md): Start app from command line using cli (imperative approach)
### [Exercise 5](exercises/kubernetes/exercise5/README.md): Show information about pod restarts
### [Exercise 6](exercises/kubernetes/exercise6/README.md): Add labels
### [Exercise 7](exercises/kubernetes/exercise7/README.md): Start application from yaml file (declarative approach)
### [Exercise 8](exercises/kubernetes/exercise8/README.md): Delete resources
### [Exercise 9](exercises/kubernetes/exercise9/README.md): Get information about all PODs configuration options
### [Exercise 10](exercises/kubernetes/exercise10/README.md): Check what default values are set for busybox POD
### [Exercise 11](exercises/kubernetes/exercise11/README.md): Get information about namespace in POD in metadata
### [Exercise 12](exercises/kubernetes/exercise12/README.md): Get information about temat horizontalpodautoscalers
### [Exercise 13](exercises/kubernetes/exercise13/README.md): Liveness and readiness probe
### [Exercise 14](exercises/kubernetes/exercise14/README.md): Add pod limits as container environment variable
### [Exercise 15](exercises/kubernetes/exercise15/README.md): Test default limits in namespace
### [Exercise 16](exercises/kubernetes/exercise16/README.md): Test qouta in namespace
### [Exercise 17](exercises/kubernetes/exercise17/README.md): Using labels in deployment
### [Exercise 18](exercises/kubernetes/exercise18/README.md): Create DeamonSeta
### [Exercise 19](exercises/kubernetes/exercise19/README.md): Test statefulset application
### [Exercise 20](exercises/kubernetes/exercise20/README.md): Cronjob with cowsay application
### [Exercise 21](exercises/kubernetes/exercise21/README.md): Create POD with configmap as volume
### [Exercise 22](exercises/kubernetes/exercise22/README.md): Rolling strategy
### [Exercise 23](exercises/kubernetes/exercise23/README.md): Recreate strategy
### [Exercise 24](exercises/kubernetes/exercise24/README.md): Blue/Green strategy
### [Exercise 25](exercises/kubernetes/exercise25/README.md): Update i rollback
### [Exercise 26](exercises/kubernetes/exercise26/README.md): Application scale
### [Exercise 27](exercises/kubernetes/exercise27/README.md): Get information about variable metrics in HorizontalPodAutoscaler
### [Exercise 28](exercises/kubernetes/exercise28/README.md): Use HPA based on CPU
### [Exercise 29](exercises/kubernetes/exercise29/README.md): Use HPA based on Memory
### [Exercise 30](exercises/kubernetes/exercise33/README.md): Pod with share storage between containers
### [Exercise 31](exercises/kubernetes/exercise34/README.md): Use revers proxy inside POD
### [Exercise 32](exercises/kubernetes/exercise35/README.md): Applicaiton with init container
### [Exercise 33](exercises/kubernetes/exercise36/README.md): Create ClusterIP service for nginx application
### [Exercise 34](exercises/kubernetes/exercise37/README.md): Craete headless service for nginx applicaion
### [Exercise 35](exercises/kubernetes/exercise38/README.md): Servis my-service -> www.nobleprog.pl
### [Exercise 36](exercises/kubernetes/exercise39/README.md): Ingress test
### [Exercise 37](exercises/kubernetes/exercise37/README.md): Test emptydir
### [Exercise 38](exercises/kubernetes/exercise38/README.md): Using PVC
### [Exercise 39](exercises/kubernetes/exercise39/README.md): LAMP application
### [Exercise 40](exercises/kubernetes/exercise40/README.md): Start EFK (Elastic - Fluentd - Kibana)
### [Exercise 41](exercises/kubernetes/exercise41/README.md): Start metric server

### [Exercise 42](exercises/kubernetes/exercise42/README.md): Create new user
### [Exercise 43](exercises/kubernetes/exercise43/README.md): Create rule for user test-user
### [Exercise 44](exercises/kubernetes/exercise44/README.md): Start pod with set UID
### [Exercise 45](exercises/kubernetes/exercise45/README.md): Turn off capabilities
### [Exercise 46](exercises/kubernetes/exercise46/README.md): Create secret
### [Exercise 47](exercises/kubernetes/exercise47/README.md): Network policy - restrictied access from pod 

### Extra 

### [Exercise 48](exercises/kubernetes/exercise48/README.md): Install Prometheus
### [Exercise 49](exercises/kubernetes/exercise49/README.md): Install grafana 
